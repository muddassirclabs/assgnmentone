/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.util;

import com.app.entities.Car;
import com.app.entities.Customer;
import com.app.entities.Hyundai;
import com.app.entities.Maruti;
import com.app.entities.Toyota;
import static com.app.util.ApplicationConstants.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author Muddassir Iqbal
 */
public class AddDetail {

    public static Map<Integer, Customer> customerDB = new HashMap<>();
    private static Set<Integer> carIDSet = new HashSet<>();
    public static Scanner scan = new Scanner(System.in);

    // Add customer detail
    public void addCustomer() {
        System.out.println("");
        scan = new Scanner(System.in);
        System.out.print("Enter customer id: ");
        if (scan.hasNextInt()) {    // Check if input is integer
            int id = scan.nextInt();
            if (customerDB.containsKey(id)) {
                System.out.println("ID already taken!");
            } else {
                if (scan.hasNextLine()) {  //check if any input string is there
                    scan.nextLine();
                    System.out.print("Enter customer name: ");
                    String name = scan.nextLine();
                    Customer obj = new Customer(id, name); // Creates new Customer object
                    customerDB.put(id, obj); // Adding Customer object to Map
                } else {
                    System.out.println("Enter correct Name!!");
                }
            }
        } else {
            System.out.println("Enter correct id.");
        }
        System.out.println("");
    }

    // Retrieve customer detail
    public Customer searchCustomerId(int id) {
        // Complexity is O(1)
        return customerDB.get(id);
    }

    // Add car details
    public void addCarToCustomer() {
        /* Complexity = Complexity of searchCustomerID()+ Complexity of contains(Object o)
         * i.e. Complexity = O(n)
         */
        System.out.println("");
        scan = new Scanner(System.in);
        Car carObj = null;
        Customer customerObj = null;
        if (customerDB.isEmpty()) { // Check if the customer Map is empty or not
            System.out.println("No Customer added yet!!");
        } else {
            System.out.print("Enter customer id: ");
            if (scan.hasNextInt()) { // Check if input is integer
                int id = scan.nextInt();
                customerObj = searchCustomerId(id);
                if (customerObj != null) { // If any object is retrieved then this will work
                    System.out.println("Enter car ID:");
                    if (scan.hasNextInt()) {  // Check if input is integer
                        int carId = scan.nextInt();
                        if (carIDSet.contains(carId)) { // Check to see if Car Id already exists
                            System.out.println("This ID already exists."
                                    + "Please enter new Car Id.");
                        } else {
                            System.out.println("1. Hyundai");
                            System.out.println("2. Maruti");
                            System.out.println("3. Toyota");
                            System.out.println("Choose Brand:");
                            if (scan.hasNextInt()) {
                                int choice = scan.nextInt();
                                switch (choice) { //Here we create car according to option
                                    case HYUNDAI_OPTION:
                                        carObj = new Hyundai(carId);
                                        break;
                                    case MARUTI_OPTION:
                                        carObj = new Maruti(carId);
                                        break;
                                    case TOYOTA_OPTION:
                                        carObj = new Toyota(carId);
                                        break;
                                    default:
                                        System.out.println("Enter correct Option.");
                                        break;
                                }
                                if (carObj != null) {
                                    carIDSet.add(carId); // Store carId for further use
                                    customerObj.getCustomerCarDB().add(carObj); // add car to customer list
                                }
                            } else {
                                System.out.println("Enter correct Option.");
                            }

                        }
                    } else {
                        System.out.println("Enter correct details!!");
                    }
                } else {
                    System.out.println("No record matched the ID.");
                }

            } else {
                System.out.println("Please enter a valid ID.");
            }

        }
        System.out.println("");
    }
}