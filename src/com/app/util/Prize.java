/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import static com.app.util.AddDetail.scan;
import java.util.HashSet;
import java.util.Scanner;

/**
 *
 * @author Muddassir Iqbal
 */
public class Prize {

    Set<Integer> luckyDraw = new HashSet<>();
    List<Integer> userDraw = new ArrayList<>();

    // this method generates 6 Lucky IDs randomly
    public void generateLuckyIDs() {
        if (AddDetail.customerDB.isEmpty() || (AddDetail.customerDB.size()<6)) {
            System.out.println("There must be 6 Customers for Lucky Draw!!");
        } else {
            Random generator = new Random();

            Object[] IDs = AddDetail.customerDB.keySet().toArray();
            while (true) {
                Object randomIDs = IDs[generator.nextInt(IDs.length)];
                luckyDraw.add(Integer.parseInt(randomIDs.toString()));
                //System.out.println(luckyDraw.toString());
                if (luckyDraw.size() == 6) {
                    break;
                }
            }
            getUserIDs();
            compareIDs();
        }
    }

    // this method takes input from the user
    public void getUserIDs() {
        scan = new Scanner(System.in);
        System.out.println("Enter your Lucky Draw");
        for (int i = 3; i > 0; i--) {
            System.out.print("Draw " + i + ": ");
            if (scan.hasNext()) {
                userDraw.add(scan.nextInt());
            } else {
                System.out.println("Enter correct ID number.");
                i++;
            }
        }
    }

    // this method matches the IDs entered by user with Randomly generated IDs
    public void compareIDs() {
        int count = 0;
        for (Integer i : userDraw) {
            if (luckyDraw.contains(i)) {
                count++;
                System.out.println("Lucky winner is with ID number: " + i);
            }
        }
        if (count > 0) {
            System.out.println("There are total " + count + " Lucky Winner(s)");
        } else {
            System.out.println("Sorry better luck next time.");
        }
    }
}
