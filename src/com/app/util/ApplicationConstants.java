/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.util;

/**
 *
 * @author Muddassir Iqbal
 */
public interface ApplicationConstants {
    // Model name contants

    String TOYOTA = "Toyota";
    String MARUTI = "Maruti";
    String HYUNDAI = "Hyundai";
    // Resale value constants
    double TOYOTA_RESALE = .80;
    double MARUTI_RESALE = .60;
    double HYUNDAI_RESALE = .40;
    //Options
    int HYUNDAI_OPTION = 1;
    int MARUTI_OPTION = 2;
    int TOYOTA_OPTION = 3;
    int ADD_CUSTOMER = 1;
    int ADD_CAR = 2;
    int SEARCH_CUSTOMER = 3;
    int VIEW_ALL = 4;
    int PRIZE = 5;
    int EXIT = 0;
}
