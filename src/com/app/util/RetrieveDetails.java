/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.util;

import com.app.entities.Car;
import static com.app.util.AddDetail.scan;
import com.app.entities.Customer;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeSet;

/**
 *
 * @author Muddassir Iqbal
 */
public class RetrieveDetails {

    public void retrieveSingleCustomer() { //Complexity is O(1*m)
        scan = new Scanner(System.in);
        if (AddDetail.customerDB.isEmpty()) {
            System.out.println("No Customer added yet!!");
        } else {
            System.out.print("Enter customer ID: ");
            if (scan.hasNextInt()) {
                int id = scan.nextInt();
                viewCustomerDetail(AddDetail.customerDB.get(id));
            }
        }
    }

    public void retrieveAllCustomer() { // Complexity is O(n*m)
        Map<Integer, Customer> sortedMap = sortByValue(AddDetail.customerDB); // to sort customer by name
        if (AddDetail.customerDB.isEmpty()) {
            System.out.println("No customer added!!");
        } else {
            for (Integer i : sortedMap.keySet()) {
                viewCustomerDetail(AddDetail.customerDB.get(i));
            }
        }
    }

    public void viewCustomerDetail(Customer cust) {
        System.out.println("ID: " + cust.getId());
        System.out.println("Name: " + cust.getName());
        System.out.println("*******************************************");

        if (cust.getCustomerCarDB().isEmpty()) {
            System.out.println("No cars added yet!!");
        } else {
            TreeSet<Car> sorted_set = new TreeSet<>(cust.getCustomerCarDB()); // Sort the car list by Model
            for (Car c : sorted_set) {
                System.out.println("Car ID: " + c.getId());
                System.out.println("Car Model: " + c.getModel());
                System.out.println("Price: " + c.getPrice());
                System.out.println("Resale Value: " + c.getResaleValue());
                System.out.println("Brand: " + c.getBrand());
                System.out.println("-----------------------------------------");
            }
        }
    }

    // This method will sort the Map of customer by customer name
    static Map sortByValue(Map<Integer, Customer> map) {
        List<Map.Entry<Integer, Customer>> list = new LinkedList<>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Customer>>() {
            @Override
            public int compare(Map.Entry<Integer, Customer> o1, Map.Entry<Integer, Customer> o2) {
                return (o1.getValue().getName().compareTo(o2.getValue().getName()));
            }
        });

        Map result = new LinkedHashMap();
        for (Map.Entry entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
