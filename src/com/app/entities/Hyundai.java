/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import com.app.util.ApplicationConstants;

/**
 *
 * @author Muddassir Iqbal
 */
public class Hyundai extends Car {

    public Hyundai(int id) {
        this.id = id;
        setBrand();
        setPrice();
        setModel();
    }

    @Override
    public void setBrand() {
        this.brand = ApplicationConstants.HYUNDAI;
    }

    @Override
    public void setResaleValue() {
        this.resaleValue = ApplicationConstants.HYUNDAI_RESALE * this.price;
    }

    @Override
    public int compareTo(Car o) {
        return this.model.compareTo(o.model);
    }
}
