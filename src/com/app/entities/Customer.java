/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Muddassir Iqbal
 */
public class Customer{

    protected int id;
    protected String name;
    protected Set<Car> customerCarDB = new HashSet<>();

    public Set<Car> getCustomerCarDB() {
        return customerCarDB;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Customer(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
