/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import com.app.util.ApplicationConstants;

/**
 *
 * @author Muddassir Iqbal
 */
public class Toyota extends Car {

    public Toyota(int id) {
        this.id = id;
        setBrand();
        setPrice();
        setModel();
    }

    @Override
    public void setBrand() {
        this.brand = ApplicationConstants.TOYOTA;
    }

    @Override
    public void setResaleValue() {
        this.resaleValue = ApplicationConstants.TOYOTA_RESALE * this.price;
    }

    @Override
    public int compareTo(Car o) {
        return this.model.compareTo(o.model);
    }
}
