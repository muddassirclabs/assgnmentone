/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import static com.app.util.AddDetail.scan;
import java.util.Scanner;

/**
 *
 * @author Muddassir Iqbal
 */
public abstract class Car implements Comparable<Car> {

    // Car attributes
    protected int id;
    protected double resaleValue;
    protected double price;
    protected String brand;
    protected String model;

    // Abstract methods
    public abstract void setBrand();

    public abstract void setResaleValue();

    // Set Price for the car
    public void setPrice() {
        do {
            scan = new Scanner(System.in);
            System.out.print("Enter the price: ");
            if (scan.hasNextDouble()) {
                this.price = scan.nextDouble();
                break;
            } else {
                System.out.println("Please enter a correct value.");
                scan.nextLine();
            }
        } while (true);
        setResaleValue();
    }

    // Set model for the car
    public void setModel() {
        do {
            scan = new Scanner(System.in);
            System.out.print("Enter the model: ");
            if (scan.hasNextLine()) {
                this.model = scan.nextLine();
                break;
            } else {
                System.out.println("Please enter a correct value.");
                scan.nextLine();
            }
        } while (true);
        
    }

    //Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getResaleValue() {
        return resaleValue;
    }

    public double getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }
}
