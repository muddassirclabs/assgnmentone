/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import com.app.util.ApplicationConstants;

/**
 *
 * @author Muddassir Iqbal
 */
public class Maruti extends Car {

    public Maruti(int id) {
        this.id = id;
        setBrand();
        setPrice();
        setModel();
    }

    @Override
    public void setBrand() {
        this.brand = ApplicationConstants.MARUTI;
    }

    @Override
    public void setResaleValue() {
        this.resaleValue = ApplicationConstants.MARUTI_RESALE * this.price;
    }

    @Override
    public int compareTo(Car o) {
        return this.model.compareTo(o.model);
    }
}
