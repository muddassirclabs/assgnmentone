/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.main;

import static com.app.util.AddDetail.scan;
import com.app.util.AddDetail;
import com.app.util.RetrieveDetails;
import static com.app.util.ApplicationConstants.*;
import com.app.util.Prize;
import java.util.Scanner;

/**
 *
 * @author Muddassir Iqbal
 */
public class Main {

    static AddDetail addDetailObj = new AddDetail();
    static Prize prizeObj = new Prize();
    static RetrieveDetails retrieveDetailObj = new RetrieveDetails();

    public static void main(String[] args) {

        int option;
        do {
            // Main Menu Options
            scan = new Scanner(System.in);
            System.out.println("1. Add Customer - O(1)");
            System.out.println("2. Add Car - O(n)");
            System.out.println("3. Search a Customer by ID - O(m)");
            System.out.println("4. View All Customer Records - O(n*m)");
            System.out.println("5. Prize(Lucky Draw) - O(n)");
            System.out.println("0. Exit System");

            System.out.print("Enter your choice: ");
            if (scan.hasNextInt()) {
                option = scan.nextInt();
            } else {
                System.out.println("Enter a correct option!!");
                scan.nextLine();
                continue;
            }
            //Switch to cases as per options
            switch (option) {
                case EXIT:
                    System.exit(0);
                    break;
                case ADD_CUSTOMER:
                    addDetailObj.addCustomer();
                    break;
                case ADD_CAR:
                    addDetailObj.addCarToCustomer();
                    break;
                case SEARCH_CUSTOMER:
                    retrieveDetailObj.retrieveSingleCustomer();
                    break;
                case VIEW_ALL:
                    retrieveDetailObj.retrieveAllCustomer();
                    break;
                case PRIZE:
                    prizeObj.generateLuckyIDs();
                    break;
                default:
                    System.out.println("Enter correct Option.");
                    break;
            }
            System.out.println("");
        } while (true);
    }
}